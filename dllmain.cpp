#include "pch.h"
#include <thread>
#include <string>
#include <filesystem>
#include <fstream>
#include "library.hpp"
#include "settings.hpp"
static int Address;



void MainFunction()
{
    if (!std::filesystem::exists("ChangeIDParentheses.ini"))
    {
        std::ofstream config("ChangeIDParentheses.ini");
        config << "[settings]\ntype=1" << std::endl;
    }

    switch (SAMP::GetSAMPVersion())
    {
    case SAMP::sampVersion::R1: Address = 0xD835C; break;
    case SAMP::sampVersion::R2: Address = 0xD8370; break;
    case SAMP::sampVersion::R3: Address = 0xEA738; break;
    case SAMP::sampVersion::R4: Address = 0xEA790; break;
    }

    DWORD protect;
    VirtualProtect(reinterpret_cast<LPVOID>(SAMP::GetSAMPHandle() + Address), 40, PAGE_EXECUTE_READWRITE, &protect);
    switch (ReadInt("settings", "type"))
    {
    case 0: std::strcpy(reinterpret_cast<char*>(SAMP::GetSAMPHandle() + Address), "%s %d"); break;
    case 1: std::strcpy(reinterpret_cast<char*>(SAMP::GetSAMPHandle() + Address), "%s (%d)"); break;
    case 2: std::strcpy(reinterpret_cast<char*>(SAMP::GetSAMPHandle() + Address), "%s [%d]"); break;
    case 3: std::strcpy(reinterpret_cast<char*>(SAMP::GetSAMPHandle() + Address), "%s {%d}"); break;
    }
    VirtualProtect(reinterpret_cast<LPVOID>(SAMP::GetSAMPHandle() + Address), 40, protect, &protect);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
    {
        DisableThreadLibraryCalls(hModule);
        std::thread(MainFunction).detach();
    }
    return TRUE;
}



