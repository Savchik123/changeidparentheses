using namespace std::string_literals;
char* Path_folder()
{
	char szPath[MAX_PATH] = {};
	GetModuleFileNameA(NULL, szPath, MAX_PATH);
	char* lstChr = strrchr(szPath, '\\');
	*lstChr = '\0';
	return szPath;
}

const char* ReadChar(std::string _Section, std::string _Key)
{
	char str[256];
	GetPrivateProfileStringA(_Section.c_str(), _Key.c_str(), NULL, str, sizeof(str), (Path_folder() + "\\ChangeIDParentheses.ini"s).c_str());
	return str;
}

int ReadInt(std::string _Section, std::string _Key)
{
	char str[256];
	GetPrivateProfileStringA(_Section.c_str(), _Key.c_str(), NULL, str, sizeof(str), (Path_folder() + "\\ChangeIDParentheses.ini"s).c_str());
	return std::stoi(str);
}
